##############################################################################################
TRGT = arm-none-eabi-
CC   = $(TRGT)gcc
CP   = $(TRGT)objcopy
AS   = $(TRGT)gcc -x assembler-with-cpp
RM   = rm
HEX  = $(CP) -O ihex 
BIN  = $(CP) -O binary
GPG = "C:/Program Files/GNU/GnuPG/gpg"

MCU  = cortex-m4
##############################################################################################

##############################################################################################
# Define project name here
PROJECT = lpc43_tcp_test

# Define linker script file here
LDSCRIPT= ./lpc4333.ld

# List all user C define here, like -D_DEBUG=1
DEFS =-DNDEBUG -DCORE_M4 -DCHIP_LPC43XX -DSTACK_SIZE=512
#DEFS += -DBOARD_MCB43000
DEFS += -DBOARD_E502

# Define ASM defines here
ADEFS = 

# List C source files here
SRC  = ./src/main.c \
       ./lib/lprintf/lprintf.c \
       ./lib/lprintf/lpc43xx_uart/lprintf_lpc43xx_uart.c \
       ./lib/lpc_chip/lpc43xx/init/lpc_init.c \
       ./lib/lpc_chip/lpc43xx/init/startup_gcc.c \
       ./lib/lip/src/examples/tcp_cntr/lip_sample_tcp_cntr.c \
       ./lib/timer/ports/cm4_systick/clock.c



# List ASM source files here
ASRC = 

# List additional system include directories here
SYSINCDIRS = \
    ./lib/lpc_chip \
    ./lib/lpc_chip/cmsis


# List all user include directories here
INCDIRS = ./src \
          ./lib/lprintf \
          ./lib/timer \
          ./lib/timer/ports/cm4_systick \
          ./lib/lcspec \
          ./lib/lcspec/gcc



LIP_TARGET := lpc43xx
LIP_MAKEFILE := ./lib/lip/lip_src.mk
LIP_ENABLE_DOC := 1
include $(LIP_MAKEFILE)
SRC += $(LIP_SRC)
INCDIRS += $(LIP_INCDIRS)

# List the user directory to look for the libraries here
LIBDIRS = 

# List all user libraries here
LIBS = 

# Define optimisation level here
OPT = -O3

##############################################################################################

INCOPT  = $(patsubst %,-isystem%,$(SYSINCDIRS)) $(patsubst %,-I%,$(INCDIRS))
LIBOPT  = $(patsubst %,-L%,$(LIBDIRS))
OBJS    = $(ASRC:.s=.o) $(SRC:.c=.o)
MCFLAGS = -mcpu=$(MCU) -mthumb
# -march=armv7e-m
# for floating point:
# -mfloat-abi=softfp -mfpu=vfpv3xd
WARNOPTS = \
    -Werror=implicit-int -Werror=implicit-function-declaration -Werror=strict-prototypes -Werror=return-type \
    -Wall -Wextra -Werror \
    -Wdouble-promotion -Wformat-security -Winit-self -Wstrict-aliasing -Wfloat-equal \
    -Wundef -Wshadow -Wunsafe-loop-optimizations -Wpointer-arith  \
    -Wwrite-strings  -Wjump-misses-init -Wsign-compare -Wlogical-op  -Winline
# Make certain warnings non-fatal (overrides global -Werror)
WARNOPTS += -Wno-error=inline -Wno-error=unused-variable -Wno-unused-parameter -Wno-error=unused-function
# Extra warnings
WARNOPTS += -Wframe-larger-than=200
#Wpadded -Wstrict-overflow=5 -Wconversion -Wcast-align -Waggregate-return

# Other possible warnings...
# -Wformat-nonliteral
# -Wno-sign-conversion

LDOPTS = -Map=$(PROJECT).map,--cref,--no-warn-mismatch

ASFLAGS = $(MCFLAGS) -g -gdwarf-2 -Wa,-amhls=$(<:.s=.lst) $(ADEFS)
CPFLAGS = $(MCFLAGS) $(OPT) -std=gnu99 -gdwarf-2 -fomit-frame-pointer -fverbose-asm \
    $(WARNOPTS) -Wa,-ahlms=$(<:.c=.lst) $(DEFS) $(INCOPT)

LDFLAGS = $(MCFLAGS) -nostartfiles -T$(LDSCRIPT) -Wl,$(LDOPTS) $(LIBOPT) -lm

# Generate dependency information
CPFLAGS += -MD -MP -MF .dep/$(@F).d

GPGFLAGS = --force-v3-sigs --digest-algo SHA256 --passphrase "lcard test passphrase" --detach-sign
 
all: $(OBJS) $(PROJECT).elf $(PROJECT).hex $(PROJECT).bin $(LIP_DOC)
	$(TRGT)size -A $(PROJECT).elf
    
%.o : %.c Makefile $(LIP_MAKEFILE)
	$(CC) -c $(CPFLAGS) $< -o $@ 	

%o : %s Makefile
	$(AS) -c $(ASFLAGS) $< -o $@

%elf: $(OBJS) $(LDSCRIPT)
	$(CC) $(OBJS) $(LDFLAGS) $(LIBS) -o $@

%.hex: %.elf
	$(HEX) $< $@

%.bin: %.elf
	$(BIN) $< $@
	
%.sig: %
	-$(RM) -f $@
	$(GPG) $(GPGFLAGS) $<

clean:
	-$(RM) -f $(OBJS)
	-$(RM) -f $(PROJECT).elf
	-$(RM) -f $(PROJECT).map
	-$(RM) -f $(PROJECT).hex
	-$(RM) -f $(PROJECT).bin
	-$(RM) -f $(PROJECT).bin.sig
	-$(RM) -f $(SRC:.c=.c.bak)
	-$(RM) -f $(SRC:.c=.lst)
	-$(RM) -f $(ASRC:.s=.s.bak)
	-$(RM) -f $(ASRC:.s=.lst)
	-$(RM) -fR .dep

# 
# Include the dependency files, should be the last of the makefile
#
-include $(shell mkdir .dep 2>/dev/null) $(wildcard .dep/*)

# *** EOF ***
