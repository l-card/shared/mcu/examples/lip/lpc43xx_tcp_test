
#include "chip.h"
#include "timer.h"
#include "lip.h"
#include "lprintf.h"
#include "examples/tcp_cntr/lip_sample_tcp_cntr.h"



#define BLINK_PIN   LPC_PIN_P2_5_GPIO5_5




#define UDP_PORT  11115

static const uint8_t mac[LIP_MAC_ADDR_SIZE] = {0x00, 0x05, 0xF7, 0xFF, 0xFF, 0xFA };
static const uint8_t ip[LIP_IPV4_ADDR_SIZE] = {192,168,12,251};
static const uint8_t mask[LIP_IPV4_ADDR_SIZE] = {255,255,255,0 };
static const uint8_t gate[LIP_IPV4_ADDR_SIZE] = {192,168,12,1};
static const int     use_dhcp = 0;

int main(void)  {

    clock_init();

    LPC_PIN_CONFIG(BLINK_PIN, 0, LPC_PIN_PULLUP);
    LPC_PIN_DIR_OUT(BLINK_PIN);





    LPC_USART3->LCR = UART_LCR_PARITY_EN | 3;
    LPC_USART3->IER = 0;


    lprintf_uart_init(3, 115200, 8, LPRINTF_UART_PARITY_NONE,  LPC_PIN_P2_3_U3_TXD, 0);



    lprintf("test porgram started!\n");

    lip_sample_tcp_cntr_init(mac, use_dhcp ? NULL : ip, mask, gate, UDP_PORT);
    t_timer tmr;
    timer_set(&tmr, CLOCK_CONF_SECOND);
    LPC_PIN_OUT(BLINK_PIN, 1);
    for (;;) {

        lip_sample_tcp_cntr_pull();
        if (timer_expired(&tmr) && (lip_state()==e_LIP_STATE_CONFIGURED)) {
            LPC_PIN_TOGGLE(BLINK_PIN);
            timer_reset(&tmr);
        }
    }
}
